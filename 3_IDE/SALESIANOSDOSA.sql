-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 27-02-2020 a las 11:39:53
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `SALESIANOSDOSA`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ALUMNOS`
--

CREATE TABLE `ALUMNOS` (
  `ID_SERIAL` bigint(20) UNSIGNED NOT NULL,
  `Nombre` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `Apellidos` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `sexo` enum('H','M','N') COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `date_control` date DEFAULT NULL,
  `Pais` varchar(20) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `archivo` blob DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `LinkedIn` varchar(30) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `telefono` int(10) UNSIGNED DEFAULT NULL,
  `cantidad` smallint(5) UNSIGNED DEFAULT NULL,
  `Provincia` varchar(30) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `Usuario` varchar(20) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `Contrasenya` varchar(20) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `ciclo` varchar(40) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `modulo` varchar(40) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `preferencias` varchar(40) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `Ingles` tinyint(4) UNSIGNED DEFAULT NULL,
  `comentario` text COLLATE utf8mb4_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `ALUMNOS`
--

INSERT INTO `ALUMNOS` (`ID_SERIAL`, `Nombre`, `Apellidos`, `sexo`, `date_control`, `Pais`, `archivo`, `email`, `LinkedIn`, `telefono`, `cantidad`, `Provincia`, `Usuario`, `Contrasenya`, `ciclo`, `modulo`, `preferencias`, `Ingles`, `comentario`) VALUES
(3, 'p1', 'PRUEBA', 'M', '1964-09-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ALUMNOS`
--
ALTER TABLE `ALUMNOS`
  ADD UNIQUE KEY `ID_SERIAL` (`ID_SERIAL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ALUMNOS`
--
ALTER TABLE `ALUMNOS`
  MODIFY `ID_SERIAL` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
