# Fases

## Analisis

- no automatizada

- requisitos funcionales y no funcionales

- organizacion de reuniones

- comunicacion analista cliente

- Especificacion de requisitos

## Diseño

- Se toman decisiones

- Modelo Funcional-Estructural

## Codificacion

- se le da forma al diseño

- realizado por programadores

## Pruebas

- Pruebas unitarias

- Pruebas de integración

- Pruebas funcionales

- Pruebas de rendimiento

## Documentacion

- Guia tecnica

- Guia de uso

- Guia de instalación

## Explotacion

- El cliente la utiliza

## Mantenimiento

- Fase mas larga

- Los cambios son:
  - perfectivos
  - Evolutivos
  - Adaptativos
  - Correctivos

# Modelos de proceso
  - En cascada
  - Modelo en v
  - Iterativo
  - Incremental
  - En espiral
  - De prototipos

# Metodologias
  - Metodologias tradicionales:
    - Fuerte planificacion
    - Dependen del analisis
    - Los cambios son complejos y costosos
  - Metodologias agiles:
    - Mas flexibles
    - Documetadion, revision y respuesta
    - Unidad y cohesion en pequeños grupos
    - Se fragmenta el trabajo en Sprints

# Herramientas de apoyo
## Framework
   Es la plataforma o estuctura que apoya al programador, es un software que ayuda a la inclusion y creacion de software facilitando la reutilizacion de codigo.

   Su mayor inconveniente es la dependencia que se crea a este con el proyecto.

## CASE
   Todas aquellas aplicaciones que intervienen que facilitan o reducen el desarrolo.

   Entre sus utilidades esta la documentacion, el testing o la autocodificacion en base al diseño.

   Los IDE se integran en este grupo.

### Clasificacion
  - Integracion
    - Juegos de herramientas (Toolkits)
    - Bancos de trabajos (Workbenchs)
  - Metodologia
    - Estructurada
    - Orientada o objetos
    - Gestor de proyectos y requisitos
    - Analisis y diseño
    - Codificacion
    - Integracion y pruebas
  - Metodologia 2
    - Upper case
    - Medium case
    - Lower case

