# SHELL NOTES

BY ***@Victor-bin***

15\10\2019
---

Si en una variable insertamos un comando al llamar la variable este se ejecutara

~~~
ejemplo=`ls`

$ejemplo
~~~

al meter las variables unas dentro de otras devemos tener cuidado con " o ' ya que en el caso de la comilla simple se anulara
~~~
p1= mundo

p2= "hola $p1"

echo $p2
hola mundo 
~~~

el caso erroneo seria:
~~~
p1= mundo

p2= 'hola $p1'

echo $p2
hola $p1
~~~

alias son variables donde no es necesario utilizar el $
~~~
alias [nombre a dar] = ["comando"]
~~~

para eliminar el alias usamos unalias
~~~
unalias [nombre dado]
~~~
Los alias son compatibles con |

# SCRIPTING NOTES

Se iniciara siempre con
~~~
\#/bin/bash
~~~

el nombre del fichearo sera \*.sh y tendra permisos de ejecucion con **chmod** o **source**

para ejecutar utilizaremos ./nombredelfichero
- - -


