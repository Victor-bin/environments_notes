#!/bin/bash
#Ejemplo 3: asignación de variables
 echo -n "Asigno sin comillas a una variable la cadena demo* y la escribo: "
 nombre=demo*
 echo $nombre
 echo ""
 echo -n "Asigno ahora con comillas la misma cadena y escribo la variable: "
 nombre='demo\*'
 echo $nombre
 echo ""
