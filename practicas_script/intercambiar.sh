#!/bin/bash

echo "se van a intercambiar los nombres de los ficheros $1 y $2"
echo "el fichero $1 contenia:"
cat $1
echo "el fichero $2 contenia:"
cat $2

cat $1 > my1tmpto2.rm
cat $2 > my2tmpto1.rm
mv my1tmpto2.rm $2
mv my2tmpto1.rm $1

echo "el fichero $1 ahora contiene:"
cat $1
echo "el fichero $2 ahora contiene:"
cat $2

