# Ejercicio práctico formulario HTML 

## Enunciado
Diseña un página web con formulario de inserción que recoja datos de alumnos con los elementos de formulario vistos hasta ahora en clase. 
Debe contener al menos:
- [x] Foto 
```html
<input type="file" name="archivo" value="image/*">
```
- [x] Nombre y Apellidos del alumno (obligatorios).
- [x] Sexo, Fecha de nacimiento.
- [x] Correo electrónico,Teléfono móvil, Dirección postal.
- [x] Provincia y País de nacimiento.
- [x] Usuario y contraseña de acceso al sistema Moodle.
- [x] TipodeCicloFP (FPB,CFGM, CFGS)
- [x] Módulo profesional(Mejor si este campo es dinámico –para nota con programación javascript-y se carga en función de la selección de tipo de ciclo). Usar los ofertados en el colegio Domingo Savio.
- [x] Preferencias profesionales (p.e. Programación, Sistemas, Bases de datos etc.). (Mejor si este campo es dinámico –para nota con programación javascript y se carga en función de la selección del módulo profesional).
- [x] URL perfil profesional LinkedIn.
- [x] Nivel de Inglés (Malo, Regular, Bueno).
- [x] Cualquier otro campo interesante que se te ocurra.
## Notas:
- [x] Intenta utilizar el mayor número de distintos tipos de campos vistos.
- [x] Agrupa campos (p.e. Datos personales, Datos de contacto,Información profesional etc) con <fieldset>.
- [x] Usar valores por defecto.
## Entrega
Enviar por correo electrónico a la dirección de correo habitual del profesor adjuntando un fichero comprimido incluyendo el trabajo.Comprime el trabajo incluyendo:
- [x] La página HTML y opcionalmente CSS y/o Javascript.