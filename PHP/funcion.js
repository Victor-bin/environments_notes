/*******************Arrays de ciclos*******************/
//el value y la cadena valdran igual
var ciclList = new Array(8)

						/***Vacio***/	
	ciclList["vacio"]=[
		"Selecciona un Ciclo"
		];

						/**FPB***/
	ciclList["fpb"]=[
		"Servicios administrativos",
		"Electricidad y Electrónica",
		"Fabricación y Montaje",
		"Informatica y Comunicaciones"
		];
	
						/***CFGM***/	
	ciclList["cfgm"]=[
		"Soldadura y calderería",
		"Instalaciones eléctricas y automaticas",
		"Mecanizado",
		"Instalaciones de telecomunicaciónes",
		"Sistemas microinformáticos y redes",
		"Mantenimiento electromecánico"
		];

	
						/***CFGS***/
	ciclList["cfgs"]=[
		"Fabricación mecánica",
		"Sistemas de Telecomunicaciones e Informáticos",
		"Administración y finanzas",
		"Educación infantil",
		"Desarrollo de aplicaciones multiplataforma",
		"Mecatronica industrial",
		"Animación de actividades físicas y deportivas",
		"Animaciones 3D, juegos y entornos interactivos"
		];

/******************************************************/

/*******************Arrays de Modulos******************/
//el value y la cadena valdran igual
var modList = new Array(13)

						/***Vacio***/	
modList["Selecciona un Ciclo"]=[
"Selecciona un Modulo"
];

						/***FPB***/
modList["Servicios administrativos"]=[
	"Ciencias aplicadas I",
	"Comunicación y Sociedad I",
	"Prevencion de riesgos laborales",
	"Ciencias aplicadas II",
	"Comunicación y Sociedad II",
	"Formacion en centros de trabajo",
	"Tecnicas administrativas básicas",
	"Archivo y Comunicación",
	"Aplicaciones básicas de ofimatica",
	"Tratamiento informático de datos"
];

modList["Electricidad y Electrónica"]=[
	"Ciencias aplicadas I",
	"Comunicación y Sociedad I",
	"Prevencion de riesgos laborales",
	"Ciencias aplicadas II",
	"Comunicación y Sociedad II",
	"Formacion en centros de trabajo",
	"Instalaciones eléctricas y domóticas",
	"Instalaciones de Telecomunicaciones",
	"Equipos eléctricos y Electrónicos",
	"Instalación y mantenimiento de redes para transmisión de datos"
];

modList["Fabricación y Montaje"]=[
	"Ciencias aplicadas I",
	"Comunicación y Sociedad I",
	"Prevencion de riesgos laborales",
	"Ciencias aplicadas II",
	"Comunicación y Sociedad II",
	"Formacion en centros de trabajo",
	"Operaciones básicas de fabricación",
	"Soldadura y carpintería metálica",
	"Carpintería de aluminio y PVC",
	"Redes de evacuación",
	"Fontanería y calefacción básica",
	"Montaje de equipos de climatización"

];

modList["Informatica y Comunicaciones"]=[
	"Ciencias aplicadas I",
	"Comunicación y Sociedad I",
	"Prevencion de riesgos laborales",
	"Ciencias aplicadas II",
	"Comunicación y Sociedad II",
	"Formacion en centros de trabajo",
	"Montaje y mantenimiento de sistemas y componentes informáticos",
	"Operaciones auxiliares para la configuración y la explotación",
	"Equipos eléctricos y Electrónicos",
	"Instalación y mantenimiento de redes para transmisión de datos"
];

						/***CFGM***/	

modList["Soldadura y calderería"]=[
	"Desarrollo asistido por ordenador",
	"Interpretación gráfica",
	"Mecanizado",
	"Metrología y ensayos",
	"Soldadura en atmósfera natural",
	"Empresa e iniciativa emprendedora",
	"Inglés técnico para grado medio",
	"Montaje",
	"Soldadura en atmósfera protegida",
	"Trazado, corte y conformado",
	"Formación en centro de trabajo"
];
modList["Instalaciones eléctricas y automaticas"]=[
	"Automatismos industriales",
	"Electrónica",
	"Electrotecnia",
	"Formación y orientación laboral",
	"Instalaciones eléctricas interiores",
	"Empresa e iniciativa emprendedora",
	"Infraestructuras comunes de telecomunicaciones en viviendas y edificios",
	"Inglés técnico para grado medio",
	"Instalaciones de distribución",
	"Instalaciones domóticas",
	"Instalaciones solares fotovoltaicas",
	"Máquinas eléctricas",
	"Formación en centros de trabajo",
];
modList["Mecanizado"]=[
	"Empresa a iniciativa emprendedora",
	"Fabricación por arranque de viruta",
	"Interpretación gráfica",
	"Mantenimiento de máquinas-herramienta",
	"Mecanizado por control numérico",
	"Procesos de mecanizado",
	"Dibujo asistido por ordenador",
	"Fabricación por abrasión, electroerosión corte y conformado, y por  procesos especiales",
	"Formación y orientación laboral",
	"Inglés técnico para grado medio",
	"Metrología y ensayos",
	"Sistemas automatizados",
	"Formación en centros de trabajo"
];
modList["Instalaciones de telecomunicaciónes"]=[
	"Circuito cerrado de televisión y seguridad electrónica",
	"Electrónica aplicada",
	"Equipos microinformáticos",
	"Formación y orientación laboral",
	"Infraestructuras comunes de telecomunicación en viviendas y edificios",
	"Instalaciones eléctricas básicas",
	"Empresa e iniciativa emprendedora",
	"Infraestructuras de redes de datos y sistemas de telefonía",
	"Inglés técnico para grado medio",
	"Instalaciones de megafonía y sonorización",
	"Instalaciones de radiocomunicaciones",
	"Instalaciones domóticas",
	"Formación en centro de trabajo"
];
modList["Sistemas microinformáticos y redes"]=[
	"Aplicaciones informáticas",
	"Formación en centros de trabajo",
	"Montaje y mantenimiento de equipos",
	"Redes locales",
	"Sistemas operativos monopuesto",
	"Aplicaciones Web",
	"Empresa e iniciativa emprendedora",
	"Inglés técnico para Grado Medio",
	"Seguridad informática",
	"Inglés técnico para grado medio",
	"Servicios en red",
	"Sistemas operativos en red",
	"Formación en centros de trabajo"
];
modList["Mantenimiento electromecánico"]=[
	"Automatismos neumáticos e hidráulicos",
	"Electricidad y automatismos eléctricos",
	"Formación y orientación laboral",
	"Técnicas de fabricación",
	"Técnicas de unión y montaje",
	"Empresa e iniciativa emprendedora",
	"Inglés técnico para Grado Medio",
	"Montaje y mantenimiento de líneas automatizadas",
	"Montaje y mantenimiento eléctrico-electrónico",
	"Montaje y mantenimiento mecánico",
	"Formación en centros de trabajo"
];


						/***CFGS***/
modList["Fabricación mecánica"]=[
	"Diseño de moldes y moldes de fundición",
	"Diseño de productos mecánicos",
	"Formación y orientación laboral",
	"Representación gráfica en fabricación mecánica",
	"Técnicas en fabricación mecánica",
	"Automatización de la fabricación",
	"Diseño de moldes para productos poliméricos",
	"Diseño de útiles de procesado de chapa y estampación",
	"Empresa e iniciativa emprendedora",
	"Inglés técnico para grado superior",
	"Proyecto de diseño de productos mecánicos",
	"Formación en centros de trabajo"
];
modList["Sistemas de Telecomunicaciones e Informáticos"]=[
	"Sistemas de telefonía",
	"Sistema de radio y televisión",
	"Arquitectura de equipos y sistemas informáticos",
	"Sistemas operativos y lenguajes de programación",
	"Sistemas telemáticos",
	"Gestión del desarrollo de sistemas de telecomunicación e informáticos",
	"Administración, gestión y comercialización en la pequeña empresa",
	"Desarrollo de sistemas de telecomunicación e informática",
	"Calidad",
	"Seguridad en las instalaciones de telecomunicación e informática",
	"Formación y Orientación Laboral.",
	"Formación en Centros de Trabajo"
];
modList["Administración y finanzas"]=[
	"Gestión de aprovisionamiento",
	"Gestión financiera",
	"Recursos humanos",
	"Contabilidad y fiscalidad",
	"Aplicaciones informáticas y operatoria de teclados",
	"Gestión comercial y servicio de atención al cliente",
	"Administración pública",
	"Productos y servicios financieros y de seguros",
	"Proyecto empresarial",
	"Formación y orientación laboral",
	"Auditoría",
	"Formación en centros de trabajo"
];
modList["Educación infantil"]=[
	"Autonomía personal y salud infantil",
	"Desarrollo cognitivo y motor",
	"Didáctica de la educación infantil",
	"El juego infantil y su metodología",
	"Formación y orientación laboral",
	"Inglés",
	"Primeros auxilios",
	"Desarrollo socioafectivo",
	"Recursos didácticos en inglés para la educación infantil",
	"Empresa e iniciativa emprendedora",	
	"Expresión y comunicación",
	"Habilidades sociales",
	"Intervención con las familias y atención a menores en riesgo",
	"Proyecto  de atención a la infancia",
	"Formación en centros de trabajo"
];
modList["Desarrollo de aplicaciones multiplataforma"]=[
	"Sistemas informáticos",
	"Bases de Datos",
	"Programación",
	"Lenguajes de marcas y sistemas de gestión de información",
	"Entornos de desarrollo",
	"Acceso a datos",
	"Desarrollo de interfaces",
	"Programación multimedia y dispositivos móviles",
	"Programación de servicios y procesos",
	"Sistemas de gestión empresarial",
	"Proyecto de desarrollo de aplicaciones multiplataforma",
	"Formación y orientación laboral",
	"Empresa e iniciativa emprendedora",
	"Formación en centros de trabajo"
];
modList["Mecatronica industrial"]=[
	"Elementos de máquinas",
	"Formación y Orientación laboral",
	"Procesos de fabricación",
	"Representación gráfica de sistemas mecatrónicos",
	"Sistemas eléctricos y electrónicos",
	"Sistemas hidráulicos y neumáticos",
	"Sistemas mecánicos",
	"Configuración de sistemas mecatrónicos",
	"Inglés técnico para grado superior",
	"Integración de sistemas mecatrónicos",
	"Procesos y gestión de mantenimiento y calidad",
	"Simulación de sistemas mecatrónicos",
	"Proyecto de mecatrónica industrial",
	"Formación en centros de trabajo"
];
modList["Animación de actividades físicas y deportivas"]=[
	"Juegos y actividades físicas recreativas para animación",
	"Actividades físico-deportivas individuales",
	"Actividades físico-deportivas de equipo",
	"Actividades físico-deportivas con implementos",
	"Fundamentos biológicos y bases del acondicionamiento físico",
	"Organización y gestión de una pequeña empresa de actividades de tiempo libre y socioeducativas",
	"Primeros auxilios y socorrismo acuático",
	"Animación y dinámica de grupos",
	"Metodología didáctica de las actividades físico-deportivas",
	"Actividades físicas para personas con discapacidad",
	"Formación y Orientación Laboral",
	"Formación en centros de trabajo"
];
modList["Animaciones 3D, juegos y entornos interactivos"]=[
	"Animación de Elementos 2D y 3D",
	"Color, Iluminación y Acabados 2D y 3D",
	"Diseño, Dibujo y Modelado para Animación",
	"Formación y Orientación Laboral",
	"Realización de Proyectos Multimedia Interactivos",
	"Desarrollo de Entornos Interactivos Multidispositivo",
	"Empresa e Iniciativa Emprendedora",
	"Inglés Técnico para Grado Superior",
	"Proyectos de Animación Audiovisual 2D y 3D",
	"Proyectos de Juegos y Entornos Interactivos",
	"Realización del Montaje y Postproducción de Audiovisuales",
	"Proyecto de Animaciones 3D, Juegos y Entornos Interactivos",
	"Formación en centros de trabajo"
];

/******************************************************************************/
			/************** FUNCION CHANGE**************/
/* Funcion que recarga la el campo (subList) de forma dinamica en funcion del campo previo (prinList) */
/* No retorna nada (void) */
/* No realiza llamadas */
/* Lamada por: ciclchange y Modchange*/

function change(prinList,subList){
 	var newOption; 

	//Bucle que limpia la lista
	while (subList.options.length > 0)
	 		subList.remove(0); 

 	//Bucle que añade las opciones a la lista (sublist)
 	for (var i=0; i<prinList.length; i++) { 
 		newOption = document.createElement("option"); 
 		newOption.value = prinList[i];   // metemos el value correspondiente 
 		newOption.text=prinList[i]; 	 // metemos el texto correspondiente 
 	
 		//Añadimos opcion a subList
 		try { 
 			subList.add(newOption);  // Para compatibiladad con Internet Explorer 
 		} 
 		catch (IE) { 
 			subList.appendChild(newOption);  // Para el resto de navegadores
 		}
 	} 
}

			/************** FUNCION MODCHANGE**************/
/* Funcion que recarga la el campo (Preferencias) de forma dinamica en funcion del campo previo (Modulo) */
/* No retorna nada (void) */
/* Llamadas : change */
/* Lamada por: ciclchange */

function Modchange(){

	var idx = document.getElementById("modulo").selectedIndex; 
	var which = document.getElementById("modulo").options[idx].value; 
	var prinList = modList[which]; 
	var subList=document.getElementById("preferencias");
	
	change(prinList,subList);
}

			/************** FUNCION CICLCHANGE**************/
/* Funcion que recarga la el campo (Modulo) de forma dinamica en funcion del campo previo (ciclo) */
/* No retorna nada (void) */
/* Llamadas : change y Modchange */
/* No es llamada por ninguna */

function ciclchange(){

	var idx = document.getElementById("ciclo").selectedIndex; 
	var which = document.getElementById("ciclo").options[idx].value; 
	var prinList = ciclList[which]; 
	var subList=document.getElementById("modulo"); 

 change(prinList,subList);

 Modchange();
}